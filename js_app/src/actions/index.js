import fetch from 'isomorphic-fetch'
import api_key from '../bin/api_key'

export const SHOW_ACCOUNT_DETAILS = "SHOW_ACCOUNT_DETAILS";
export const HIDE_ACCOUNT_DETAILS = "HIDE_ACCOUNT_DETAILS";
export const FILTER_ACCOUNT_DETAILS = "FILTER_ACCOUNT_DETAILS";

export const showAccountDetails = (account_details) => {
    return {
        type: SHOW_ACCOUNT_DETAILS,
        account_details: account_details
    }
};

export const hideAccountDetails = (account_list) => {
    return {
        type: HIDE_ACCOUNT_DETAILS,
        account_list: account_list
    }
};

export const filterAccountList = (account_list_filter) => {
    return  {
        type: FILTER_ACCOUNT_DETAILS,
        account_list_filter: account_list_filter
    };
};

export const getAccountList = () => {
    return function(dispatch) {
        fetch('/accounts/', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key':api_key
            }
        }).then((response) => {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(jsonData => {
            dispatch(hideAccountDetails(jsonData));
        }).catch(err => {
            console.log(err);
        });
        return null;
    }
};

export const getAccountDetails = (account_number) => {
    return function(dispatch) {
        fetch('/transactions/'+account_number+'/', {
            method: 'get',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'x-api-key':api_key
            }
        }).then((response) => {
            if (response.status >= 400) {
                throw new Error("Bad response from server");
            }
            return response.json();
        }).then(jsonData => {
            dispatch(showAccountDetails(jsonData.data))
        }).catch(err => {
            console.log(err);
        });
        return null;
    }
};