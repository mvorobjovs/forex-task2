import { SHOW_ACCOUNT_DETAILS, HIDE_ACCOUNT_DETAILS, FILTER_ACCOUNT_DETAILS } from '../actions'


const initialState = {
    account_list: [],
    account_details: false,
    account_list_filter: ''
};

const filter = (account_list, filter) => {
    return account_list.map((account) => {
        account.hide = !account.account_num.includes(filter);
        return account;
    });
};

export default function (state = initialState, action) {
    if (typeof state === 'undefined') {
        return initialState
    }

    switch (action.type) {
        case FILTER_ACCOUNT_DETAILS:
            return Object.assign({}, state, {
                account_list_filter: action.account_list_filter,
                account_list: filter(state.account_list, action.account_list_filter)
            });
        case SHOW_ACCOUNT_DETAILS:
            return Object.assign({}, state, {
                account_details: action.account_details
            });
        case HIDE_ACCOUNT_DETAILS:
            return Object.assign({}, state, {
                account_details: false,
                account_list: filter(action.account_list, state.account_list_filter)
            });
        default:
            return state
    }
}