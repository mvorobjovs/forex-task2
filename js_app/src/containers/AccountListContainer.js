import { connect } from 'react-redux'
import { getAccountDetails, getAccountList, filterAccountList } from '../actions'
import AccountList from '../components/account_list/AccountList'

const filter_accounts = (account_list, account_list_filter) => {
    return account_list.filter(account => {
        return account.account_num.includes(account_list_filter);
    })
};

const mapStateToProps = (state) => {
    return {
        account_list: filter_accounts(state.account_list, state.account_list_filter),
        account_details: state.account_details,
        account_list_filter: state.account_list_filter
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onAccountItemClick: (account_number) => {
            getAccountDetails(account_number)(dispatch);
        },
        onDidMount: () => {
            getAccountList()(dispatch);
        },
        onFilterChange: (account_list_filter) => {
            dispatch(filterAccountList(account_list_filter));
        }
    }
};

const AccountListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(AccountList);

export default AccountListContainer