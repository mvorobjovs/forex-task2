import { connect } from 'react-redux'
import { getAccountList } from '../actions'
import Header from '../components/header/Header'


const mapStateToProps = (state) => {
    return {
        account_details: state.account_details
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        onGoBackClick: () => {
            getAccountList()(dispatch);
        }
    }
};

const HeaderContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(Header);

export default HeaderContainer