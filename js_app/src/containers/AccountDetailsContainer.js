import { connect } from 'react-redux'
import AccountDetails from '../components/account_details/AccountDetails'

const mapStateToProps = (state) => {
    return {
        account_details: state.account_details
    }
};

const AccountListContainer = connect(
    mapStateToProps,
    //mapDispatchToProps
)(AccountDetails);

export default AccountListContainer