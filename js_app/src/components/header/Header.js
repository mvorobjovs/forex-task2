import React from 'react'

const Header = ({ account_details, onGoBackClick }) => (
    <div className="header clearfix">
        <ul className="nav nav-pills pull-right">
            {
                account_details ?
                    <li role="presentation">
                        <button onClick={ onGoBackClick } type="button" className="btn btn-primary">
                            Go Back
                        </button>
                    </li>
                    :
                    ''
            }
        </ul>
        <h3 className="text-muted">Transaction App</h3>
    </div>
);

export default Header