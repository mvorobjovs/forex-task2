import React from 'react'
import Transaction from './Transaction'

const AccountDetails = ({ account_details }) => {
    if (account_details) {
        return(
            <div>
                <div className="row">
                    <div className="col-md-12">
                        <dl className="dl-horizontal">
                            <dt>Account Number</dt>
                            <dd>{ account_details.account_number }</dd>
                            <dt>Balance</dt>
                            <dd>{ account_details.balance } { account_details.currency }</dd>
                        </dl>
                    </div>
                </div>
                <table className="table">
                    <thead>
                    <tr>
                        <th>&nbsp;</th>
                        <th>Source</th>
                        <th>Destination</th>
                        <th>Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                        {account_details.transactions.map(transaction =>
                                <Transaction
                                    key={ 'transaction' + transaction.id }
                                    account_number={ account_details.account_number }
                                    {...transaction}
                                />
                            )
                        }
                    </tbody>
                </table>
            </div>
        )
    } else {
        return <div></div>;
    }
};

export default AccountDetails