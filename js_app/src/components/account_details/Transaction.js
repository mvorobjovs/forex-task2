import React from 'react'

const Transaction = ({ account_number, amount, destination, source }) => (
    <tr>
        <td>{ type(account_number, source) }</td>
        <td>{ destination }</td>
        <td>{ source }</td>
        <td>{ amount }</td>
    </tr>
);

var type = (account_number, source) => {
    if (account_number == source) {
        return (<span className="glyphicon glyphicon-triangle-left text-danger" aria-hidden="true"> </span>)
    } else {
        return (<span className="glyphicon glyphicon-triangle-right text-success" aria-hidden="true"> </span>)
    }
};

export default Transaction