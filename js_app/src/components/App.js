import React from 'react'
import AccountListContainer from '../containers/AccountListContainer'
import HeaderContainer from '../containers/HeaderContainer'
import AccountDetailsContainer from '../containers/AccountDetailsContainer'




const App = () => (
    <div>
        <HeaderContainer />
        <AccountListContainer />
        <AccountDetailsContainer />
    </div>
);

export default App