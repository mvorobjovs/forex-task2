import React from 'react'

const Filter = ({ account_list_filter, onFilterChange }) => {
    return (
        <div className="form-group">
            <label className="sr-only" htmlFor="filter-input">Email address</label>
            <input onChange={ event => { onFilterChange(event.target.value) } } type="text" className="form-control"
                   id="filter-input" placeholder="Account Number Filter" value={ account_list_filter }/>
        </div>
    )
};

export default Filter