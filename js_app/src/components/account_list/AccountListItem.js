import React from 'react'

const AccountListItem = ({ account_num, balance, currency, onAccountItemClick }) => (
    <tr onClick={onAccountItemClick}>
        <td>{ account_num }</td>
        <td>{ balance } { currency }</td>
    </tr>
);

export default AccountListItem