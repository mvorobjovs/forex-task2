import React, { Component } from 'react'
import AccountListItem from './AccountListItem'
import AccountListFilter from './AccountListFilter'


class AccountList extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        const { onDidMount } = this.props;
        onDidMount();
    }

    render() {
        const { account_list, account_details, account_list_filter, onAccountItemClick, onFilterChange } = this.props;
        return (
            <div>
                {
                    !account_details ?
                        <div>
                            <AccountListFilter
                                account_list_filter={ account_list_filter }
                                onFilterChange={ onFilterChange }
                            />
                            <table className="table table-hover">
                                <thead>
                                <tr>
                                    <th>Account Number</th>
                                    <th>Balance</th>
                                </tr>
                                </thead>
                                <tbody>
                                {account_list.map(account =>
                                    <AccountListItem
                                        key={ account.account_num }
                                        {...account}
                                        onAccountItemClick={() => onAccountItemClick(account.account_num)}
                                    />
                                )}
                                </tbody>
                            </table>
                        </div>
                        :
                        ''
                }
            </div>
        )
    }
}

export default AccountList