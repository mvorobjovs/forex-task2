# README #

This is an implementation of Task 2 by Maksims Vorobjovs, ma@ksi.ms

### Implemented using ###

0. Python + Django + Django Rest Framework
1. ReactJs + Redux

# HOW TO INSTALL AND RUN PYTHON ENV #

0. `$ pip install virtualenv`
1. `$ mkdir django_virtualenv`
2. `$ cd django_virtualenv/`
3. `$ git clone https://mvorobjovs@bitbucket.org/mvorobjovs/forex-task2.git`
4. `$ cd forex-task2`
5. `$ source bin/activate`
6. `$ pip install -r requirements.txt`
7. `$ cd static`
8. `$ bower install`
9. `$ cd ..`
10. `$ python manage.py runserver`

# HOW TO SETUP DATABASE  #

Only in case db.sqlite3 is removed

0. `$ python manage.py migrate`
1. `$ sqlite3 db.sqlite3 < insert_currency.sql`

# JAVASCRIPT SOURCE #

Only in case /statis/bundle.js is missing.

Source is located in js_app.

Please `$ npm install` in js_app if required.
And `$ webpack` to create bundle.

# HTTPIE Reference #

Please refer to some API usage via [Httpie](https://github.com/jkbrzt/httpie)

* `$ http GET http://127.0.0.1:8000/accounts/ x-api-key:forexkey123` get account list
* `$ http -f POST http://127.0.0.1:8000/accounts/ x-api-key:forexkey123 currency=GBP` create an account
* `$ http GET http://127.0.0.1:8000/transactions/00000001/ x-api-key:forexkey123` get account's transactions
* `$ http -f POST http://127.0.0.1:8000/transactions/ x-api-key:forexkey123 source=00000001 destination=00000002 amount=10.99` make a transfer
* `$ http -f POST http://127.0.0.1:8000/transactions/ x-api-key:forexkey123 source=00000001 amount=10.99` withdraw
* `$ http -f POST http://127.0.0.1:8000/transactions/ x-api-key:forexkey123 destination=00000001 amount=10.99` deposit

## Insert currencies into DB ##

* insert into app_currency (iso) values ('USD');
* insert into app_currency (iso) values ('EUR');
* insert into app_currency (iso) values ('GBP');
* insert into app_currency (iso) values ('CHF');

Or

`$ sqlite3 db.sqlite3 < insert_currency.sql` -> already mentioned above in HOW TO SETUP DATABASE section