"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))

from django.conf.urls import url
from django.contrib import admin

urlpatterns = [
    url(r'^admin/', admin.site.urls),
]
"""

from django.conf.urls import url, include
from rest_framework import routers
from project.app import views
from django.views.generic.base import TemplateView, RedirectView

router = routers.DefaultRouter()

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
favicon_view = RedirectView.as_view(url='/static/favicon.ico', permanent=True)

urlpatterns = [
    # serve index.html
    url(r'^$', TemplateView.as_view(template_name='index.html'), name="home"),
    # server favicon.ico
    url(r'^favicon\.ico$', favicon_view),

    # API related urls
    url(r'^accounts/$', views.Accounts.as_view()),
    url(r'^transactions/$', views.Transactions.as_view()),
    url(r'^transactions/(?P<account_num>[0-9]*)/$', views.TransactionList.as_view()),
]