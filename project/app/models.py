from django.core.validators import RegexValidator
from django.db import models
from django.core.validators import MinLengthValidator
from decimal import Decimal

from project.app.exceptions.exception_handler import RestException

class Currency(models.Model):
    iso = models.CharField(
        blank=False,
        max_length=3,
        validators=[
            MinLengthValidator(3),
        ],
        primary_key=True,
    )

class Account(models.Model):
    ACCOUNT_LENGTH = 8

    account_num = models.CharField(
        RegexValidator(
            regex='^\d{'+str(ACCOUNT_LENGTH)+'}$',
            message='Accounts are identified by '
                    +str(ACCOUNT_LENGTH)+'-digit numbers',
            code='invalid_account_number'
        ),
        max_length=ACCOUNT_LENGTH,
        unique=True
    )
    currency = models.ForeignKey(
        Currency,
        default=None,
        related_name='currency'
    )
    balance = models.DecimalField(
        max_digits=8,
        decimal_places=2,
        default=0
    )

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        super(Account, self).save()
        # saving account in two steps in case account has missing account num
        if not self.account_num:
            # account number is created by pre-filling account id with zeros
            self.account_num = str(self.id).zfill(self.ACCOUNT_LENGTH)
            super(Account, self).save()

    def withdraw(self, amount):
        if self.balance >= amount:
            self.balance = self.balance - amount
            self.save()
        else:
             raise RestException('TRANSACTION_INSUFFICIENT_BALANCE_TO_WITHDRAW')
        return self.balance

    def deposit(self, amount):
        self.balance = self.balance + Decimal(amount)
        self.save()
        return self.balance

class Transaction(models.Model):
    source = models.ForeignKey(
        Account,
        default=None,
        related_name='source',
        to_field='account_num',
        blank=True,
        null=True
    )
    destination = models.ForeignKey(
        Account,
        default=None,
        related_name='destination',
        to_field='account_num',
        blank = True,
        null = True
    )
    amount = models.DecimalField(max_digits=8, decimal_places=2)
    date = models.DateTimeField(auto_now_add=True)

    # at destination or source should be available
    def clean(self):
        if not (self.destination or self.source):
            raise RestException('TRANSACTION_DEFINE_SOURCE_AND_DESTINATION')
        if self.destination == self.source:
            raise RestException('TRANSACTION_SOURCE_EQUALS_DESTINATION')
