import json

from django.http import HttpResponse

from project.app.exceptions.error_dict import errors
from rest_framework.views import exception_handler
from rest_framework.exceptions import APIException


def rest_exception_handler(exception, context):
    """
    Custom exception handler for Django Rest Framework
    """
    response = exception_handler(exception, context)
    try:
        detail = response.data['detail']
    except AttributeError:
        detail = getattr(exception,'message','Unknown Exception'),

    if hasattr(exception, 'machine_code'):
        code = exception.machine_code
    elif hasattr(exception, 'status_code'):
        code = 'HTTP_ERROR_' + str(exception.status_code)
    else:
        code = 'UNKNOWN_EXCEPTION'

    response = HttpResponse(
        json.dumps({
            'code': code,
            'error': True,
            'message': detail
        }),
        content_type="application/json", status=getattr(exception,
                                                        'status_code', 500),
    )
    return response

class RestException(APIException):
    def __init__(self, machine_code, details = None):
        self.machine_code = machine_code
        super(RestException,self).__init__(
            errors[machine_code] if details is None else details
        )
