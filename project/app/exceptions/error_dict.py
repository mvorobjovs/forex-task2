errors = {
    'UNKNOWN_EXCEPTION':
        'Unknown Exception',
    # Model specific/ validation errors
    'TRANSACTION_DEFINE_SOURCE_AND_DESTINATION':
        'Source or destination is not defined',
    'TRANSACTION_SOURCE_EQUALS_DESTINATION':
        'Source and destination cannot be the same',
    'TRANSACTION_INSUFFICIENT_BALANCE_AT_SOURCE_ACCOUNT':
        'Source account balance is too low',
    'TRANSACTION_INSUFFICIENT_BALANCE_TO_WITHDRAW':
        'Cannot withdraw. Balance is too low',
    'VALIDATION_ERROR':
        'Validation error',
    'NO_ACCOUNT':
        'Account does not exist',
    # Transacaction and fixer.io related errors
    'FIXERIO_COULD_NOT_GET_EXTERNAL_JSON':
        'Fixer IO is not reachable',
    'FIXERIO_COULD_NOT_PARSE_RESPONSE':
        'Fixer IO response could not be parsed',
    'TRANSACTION_DESTINATION_OR_SOURCE_ARE_NOT_SET':
        'Destination or source are not set'
}
