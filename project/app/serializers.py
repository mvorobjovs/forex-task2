from models import Currency, Account, Transaction
from rest_framework import serializers
from project.app.bin.transaction.transaction_emitter import TransactionEmitter

class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('iso',)

class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('account_num', 'balance', 'currency',)

# Only currency attribute is required when account is being created
class AccountCreationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ('currency',)

class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction
        fields = ('source', 'destination', 'amount', 'id', 'date')

    # self.clean is not being called in models.Model when using
    def validate(self, attrs):
        instance = Transaction(**attrs)
        instance.clean()
        return attrs

    # transaction creation includes also withdrawal and deposit of accounts
    def create(self, validated_data):
        result = super(TransactionSerializer, self).create(validated_data)
        """
        if result.source:
            result.source.withdraw(result.amount)
        if result.destination:
            result.destination.deposit(result.amount)
        """
        transaction = TransactionEmitter(result.source, result.destination,
                                         result.amount)
        transaction.transfer()
        return result
