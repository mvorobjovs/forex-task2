import urllib2
import json
from decimal import Decimal
from django.conf import settings
from project.app.exceptions.exception_handler import RestException

class FixerIO:
    def __init__(self, convert_from, convert_to):
        self.convert_from = convert_from
        self.convert_to = convert_to
        url = settings.FIXER_IO_URL
        try:
            response = urllib2.urlopen(url + '?base=' + convert_from
                                       + '&symbols=' + convert_to)
        except:
            raise RestException('FIXERIO_COULD_NOT_GET_EXTERNAL_JSON')

        try:
            self.rates = json.load(response)
            self.rate = self.rates['rates'][convert_to]
        except:
            raise RestException('FIXERIO_COULD_NOT_PARSE_RESPONSE')

    def convert(self, amount):
        return round(Decimal(self.rate) * amount, 2)
