from fixer_io import FixerIO
from project.app.exceptions.exception_handler import RestException


class TransactionEmitter:
    def __init__(self, source, destination, amount):
        self.source = source
        self.destination = destination
        self.withdraw_amount = amount
        self.deposit_amount = amount

    def transfer(self):
        if self.source and self.destination:
            self.deposit_amount = self.get_converted_amount(self.deposit_amount)
            self.destination.deposit(self.deposit_amount)
            self.source.withdraw(self.withdraw_amount)
        elif self.source:
            self.source.withdraw(self.withdraw_amount)
        elif self.destination:
            self.destination.deposit(self.deposit_amount)
        else:
            raise RestException('TRANSACTION_DESTINATION_OR_SOURCE_ARE_NOT_SET')

    # In case currencies of source and destination are different
    def get_converted_amount(self, amount):
        if not self.source.currency == self.destination.currency:
            fixer_io = FixerIO(self.source.currency.iso,
                               self.destination.currency.iso)
            return fixer_io.convert(amount)
        else:
            return amount
