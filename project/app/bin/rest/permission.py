from rest_framework import permissions
from rest_framework import exceptions
from django.conf import settings


# checks if there is x-api-key in the header of the request
class ApiKeyAuthentication(permissions.BasePermission):
        def has_permission(self, request, view):
            """
            Return `True` if permission is granted, `False` otherwise.
            """
            api_key = request.META.get('HTTP_X_API_KEY')
            if not api_key == getattr(settings, "REST_FRAMEWORK", None)['API_KEY']:
                raise exceptions.PermissionDenied('Wrong API Key')
            else:
                return True
