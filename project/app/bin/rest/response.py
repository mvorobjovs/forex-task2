from rest_framework.response import Response
from rest_framework import status

class Response200(Response):
    def __init__(self, data):

        data = {
            'error': False,
            'data': data
        }
        super(Response200, self).__init__(data, status=status.HTTP_200_OK)

class Response400(Response):

    def __init__(self, code, message):
        data = {
            'error': True,
            'code': code,
            'message': message
        }
        super(Response400, self).__init__(data,
                                          status=status.HTTP_400_BAD_REQUEST)
