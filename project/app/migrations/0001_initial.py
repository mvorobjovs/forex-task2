# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-09-02 15:49
from __future__ import unicode_literals

import django.core.validators
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('account_num', models.CharField(max_length=8, unique=True, verbose_name=django.core.validators.RegexValidator(code=b'invalid_account_number', message=b'Accounts are identified by 8-digit numbers', regex=b'^\\d{8}$'))),
                ('balance', models.DecimalField(decimal_places=2, default=0, max_digits=8)),
            ],
        ),
        migrations.CreateModel(
            name='Currency',
            fields=[
                ('iso', models.CharField(max_length=3, primary_key=True, serialize=False, validators=[django.core.validators.MinLengthValidator(3)])),
            ],
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.DecimalField(decimal_places=2, max_digits=8)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('destination', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='destination', to='app.Account', to_field=b'account_num')),
                ('source', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='source', to='app.Account', to_field=b'account_num')),
            ],
        ),
        migrations.AddField(
            model_name='account',
            name='currency',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.CASCADE, related_name='currency', to='app.Currency'),
        ),
    ]
