from django.db.models import Q

from project.app.models import Account, Transaction
from project.app.exceptions.exception_handler import RestException
from project.app.serializers import \
    AccountSerializer, AccountCreationSerializer, TransactionSerializer
from project.app.bin.rest.response import Response200, Response400
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status


class Accounts(APIView):
    # List all accounts
    def get(self, request):
        accounts = Account.objects.all()
        account_serializer = AccountSerializer(accounts, many=True)
        return Response(account_serializer.data)

    # Create account
    def post(self, request):
        serializer = AccountCreationSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response200({
                'account_number': serializer.instance.account_num
            })
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class TransactionList(APIView):
    def get_object(self, account_num):
        try:
            return Account.objects.get(account_num=account_num)
        except Account.DoesNotExist:
            raise RestException('NO_ACCOUNT')

    def get(self, request, account_num):
        account = self.get_object(account_num)
        serializer = AccountSerializer(account)

        transactions = Transaction.objects.filter(
            Q(source__exact=account_num) |
            Q(destination__exact=account_num)
        )
        transaction_serializer = TransactionSerializer(transactions, many=True)

        return Response200({
            'account_number': serializer.instance.account_num,
            'currency': serializer.instance.currency.iso,
            'balance': serializer.instance.balance,
            'transactions': transaction_serializer.data
        })

class Transactions(APIView):
    # Perform transaction
    def post(self, request):
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response200({'transactionId': serializer.instance.id})
        return Response400('VALIDATION_ERROR', serializer.errors)